<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/view_document/{document}', 'HomeController@viewDocument')->name('view_document');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/get_doc/{type_doc}', 'HomeController@showDocument')->name('show_document');
Route::post('/add_doc', 'HomeController@uploadDocument' )->name('add_doc');
Route::get('/destroy_document/{document}', 'HomeController@destroyDocument' )->name('destroy_document');