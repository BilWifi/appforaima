@extends('layouts.master')
@include('partials.includes.dataTables.dataTables')
@include('partials.includes.dataTables.buttons')

@section('content')
<div class="row">
    <div class="col-5">
        @foreach(App\Models\Type_document::get() as $doc)
            <a href="{{ route('show_document',$doc->idtype_documents) }}" class="text-monospace btn btn btn-dark  btn-lg  btn-block" >{{ $doc->lib }}</a>
        @endforeach
    </div>
    <div class="col-7">
        <div class="row d-flex justify-content-center">
            <div class="col-12">
                <button href="" type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal" >
                      <i class="fa fa-plus"></i> Nouveau document
                    </button>
                    <br>
                    <br>
                <div class="card">

                    {{-- <div class="card-header">DDDDD</div> --}}
                    <div class="card-body bg-dark text-white">
                      @include('partials._msgFlash')
                        <div class="row">
                            <div class="col">
                                <!-- Button trigger modal -->
                                {{-- <button href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >
                                  <i class="fa fa-plus"></i> Nouveau document
                                </button> --}}
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content bg-dark">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ajouter un doucument</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body ">
                                @include('partials._msgFlash')
                                   <form  method="POST" id="myForm" action="{{ route('add_doc') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                      <label for="titre">Titre</label>
                                      <input type="text" class="form-control" id="titre" placeholder="" name="titre" value="{{ old('titre') }}" required="">
                                    </div>
                                    <div class="form-group">
                                      <label for="description">Description</label>
                                      <input type="text" class="form-control" id="description" placeholder="" name="description" value="{{ old('description') }}" required="">
                                    </div>
                                    {{-- <input type="text" name="idtype_documents" value="{{ $type_doc->idtype_documents }}" hidden=""> --}}
                                    <div class="form-group">
                                    <label for="iddepartement">Type de de document</label>
                                    <select class="form-control" id="idtype_documents" required="" name="idtype_documents" required="">
                                      @foreach(App\Models\Type_document::get() as $d)
                                      <option value="{{ $d->idtype_documents }}">{{ $d->lib }}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                  <label for="description">Importer le fichier</label>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="doc-file" name="document"required>
                                      <label class="custom-file-label" for="doc-file">Choisir un fichier pdf</label>
                                    </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                              </div>
                               </form>
                              {{-- <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                              </div> --}}
                            </div>
                          </div>
                        </div>

                         {!!$dataTable->table() !!}

                    </div>
                </div>
                    
                    
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
  {!!$dataTable->scripts() !!}
  <script type="text/javascript">

  </script>
@endpush
























