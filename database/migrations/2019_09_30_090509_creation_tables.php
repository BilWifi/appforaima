<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_documents', function (Blueprint $table) {
            $table->bigIncrements('idtype_documents');
            $table->string('lib');
        });

        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('iddocuments');
            $table->string('titre');
            $table->text('description')->nullable();
            $table->text('file');
            $table->unsignedBigInteger('idtype_documents');
            $table->foreign('idtype_documents')
                  ->references('idtype_documents')->on('type_documents')->onDelete('cascade');
            $table->timestamps();
            

        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->string('postnom');
            $table->string('prenom');
            $table->string('pseudo')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
