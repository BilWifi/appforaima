<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\ListDocumentDataTable;

use App\Http\Requests\AddDocumentRequest;

use App\Models\Type_document;
use App\Models\Document;

use Flashy;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function viewDocument(Document $document){
        return view('view_document',compact('document'));
    }

    public function showDocument(Type_document $type_doc,ListDocumentDataTable $dataTable){
        return $dataTable->with(["idtype_documents"=>$type_doc->idtype_documents])->render('show_list_document',compact('type_doc'));
    }
        public function uploadDocument(AddDocumentRequest $request)
    {
        $file = $request->document->getClientOriginalName();
        $filename = time().'.'.$request->file('document')->guessExtension();
        // $file_name = explode('.',$file)[0]; //on recupere le nom sans l'extention

        // On verifie si le bulletin est en format pdf
        if($request->document->guessClientExtension() != 'pdf' ){
            return response()->json(['error'=>'Extension invalide pour le fichier "'.$file.'". Seules les extensions "pdf" sont autorisées.']);
        }
 
        // $path = storage_path('app/public/documents');
        $path_file  =  $request->file('document')->storeAs('documents', $filename);

        // on stoque dans la BD
        $data = request()->except('document');
        $data['file'] = $path_file;
        $doc = Document::UpdateOrCreate([
                                    'titre'=>$request->titre,
                                    
                                ],
                                
                                $data
                            );
        if (request()->ajax()) {
            return response()->json(['uploaded'=>'Document uploader avec succès']);
        }
       
        Flashy::info('Document ajouté avec succès!');
        return redirect()->route('view_document',$doc->iddocuments);
    }

    public function destroyDocument(Document $document){
        $document->delete();
        Flashy::error('Document supprimé!');
        return redirect()->back();
    }
}
