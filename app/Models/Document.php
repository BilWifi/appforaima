<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $primaryKey = 'iddocuments';
    protected $fillable = [
        'titre', 'description', 'idtype_documents','file',
    ];
    
}
